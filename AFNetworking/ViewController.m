//
//  ViewController.m
//  AFNetworking
//
//  Created by Niteesh Sharma on 11/24/15.
//  Copyright (c) 2015 Niteesh Sharma. All rights reserved.
//

#import "ViewController.h"
NSMutableArray *myArray;
@interface ViewController ()
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ViewController
@synthesize tableView;
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"working");
    [self getApi];
   //[self postApi];
    tableView.delegate =self;
    tableView.dataSource=self;
    
     myArray=[[NSMutableArray alloc]initWithObjects:@"john",@"king",@"car",@"bus",@"truck"@"trees",@"dust",@"hacker",@"counter strike" , nil];
    [tableView reloadData];
}


-(void)postApi{
    
    
    NSDictionary *params = @{@"key":@"293a64eef15b452047a0315ecc6eff2a9bbd4a9c"};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"https://www.hostedredmine.com/projects/taskconnector-test/issues.json"]
       parameters:params
          success:^(AFHTTPRequestOperation *operation, id json) {
              if (json) {
                  NSLog(@"%@",json);
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error.description);
              NSLog(@"ERROR: %@ \n\n 1. ",error.localizedDescription);
              
              
              
          }];
}

-(void) getApi{
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/forecast/daily?q=London&mode=xml&units=metric&cnt=7&appid=2de143494c0b295cca9337e1e96b00e0"]
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id json) {
             if (json) {
                 NSLog(@"%@",json);
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error.description);
             NSLog(@"ERROR: %@ \n\n 1. ",error.localizedDescription);
             
             
             
         }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [myArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSString *myString=@"Mycells11TableViewCell";
    UITableViewCell *Cell=[tableView dequeueReusableCellWithIdentifier:myString];
    Cell.textLabel.text=[myArray objectAtIndex:indexPath.row];
    //NSLog(@"%@",myArray);
    
    return Cell;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end