//
//  AppDelegate.h
//  AFNetworking
//
//  Created by Niteesh Sharma on 11/24/15.
//  Copyright (c) 2015 Niteesh Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

